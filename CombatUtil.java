package com.osroyale.game.world.entity.combat;

import com.osroyale.game.world.entity.actor.Actor;
import com.osroyale.game.world.entity.actor.npc.Npc;
import com.osroyale.game.world.entity.actor.player.Player;
import com.osroyale.game.world.position.Area;

public class CombatUtil {

    public static boolean canAttack(Actor attacker, Actor defender) {
        if (attacker.isPlayer()) {
            return canAttack(attacker.getPlayer(), defender);
        } else if (attacker.isNpc()) {
            return canAttack(attacker.getNpc(), defender);
        }
        return false;
    }

    private static boolean canAttack(Player attacker, Actor defender) {
        if (!Area.inMulti(attacker)) {
            if (!attacker.getCombat().canTarget(defender)) {
                attacker.message("You are already under attack!");
                return false;
            }
        }

        if (!Area.inMulti(defender)) {
            if (!defender.getCombat().canTarget(attacker)) {
                attacker.message(defender.getName() + " is already under attack!");
                return false;
            }
        }

        return validate(attacker) && validate(defender);
    }

    private static boolean canAttack(Npc attacker, Actor defender) {
        if (!Area.inMulti(attacker)) {
            if (!attacker.getCombat().canTarget(defender)) {
                return false;
            }
        }

        if (!Area.inMulti(defender)) {
            if (!defender.getCombat().canTarget(attacker)) {
                return false;
            }
        }

        return validate(attacker) && validate(defender);
    }

    private static boolean validate(Actor actor) {
        return !actor.isDead() && actor.getCurrentHealth() > 0 && actor.isValid() && actor.isVisible();
    }

}
