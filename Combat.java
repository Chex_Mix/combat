package com.osroyale.game.world.entity.combat;

import com.osroyale.game.world.entity.actor.Actor;
import com.osroyale.game.world.entity.combat.formula.CombatType;
import com.osroyale.game.world.entity.combat.formula.modifier.CombatRoll;
import com.osroyale.game.world.entity.combat.formula.modifier.CombatRollMap;
import com.osroyale.game.world.entity.combat.formula.modifier.RollModifier;
import com.osroyale.game.world.entity.combat.formula.stance.FightStyle;
import com.osroyale.game.world.entity.combat.formula.stance.FightType;
import com.osroyale.game.world.entity.combat.hit.Hit;
import com.osroyale.game.world.entity.combat.strategy.CombatStrategy;
import com.osroyale.game.world.entity.skill.Skill;
import com.osroyale.util.Stopwatch;

import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;

/**
 * Allows a actor to fight other actors.
 *
 * @param <T> the actor type
 * @author Michael | Chex
 */
public class Combat<T extends Actor> {

    /** The attack cooldown. */
    private int cooldown;

    /** The source actor. */
    private final T source;

    /** The current target fought against. */
    private Actor target;

    /** The most recent attacking actor. */
    private Actor lastAttacker;

    /** The fight style. */
    private FightStyle fightStyle = FightStyle.EMPTY;

    /** The fight type. */
    private FightType fightType;

    /** The current combat strategy to use for combat. */
    private CombatStrategy<? super T> strategy;

    /** The combat roll modifier map. */
    private CombatRollMap<T> formulaModifiers = new CombatRollMap<>();

    /** The elapsed time in combat. */
    private final Stopwatch elapsed = Stopwatch.start(-CombatConstants.LOGOUT_TIMEOUT);

    /** A linked list of hits that are queued to deal damage. */
    private final Queue<Hit> hitQueue = new LinkedList<>();

    /** Toggles retaliation. */
    private boolean retaliate = true;

    /**
     * Constructs the combat object.
     *
     * @param source the attacking actor
     */
    public Combat(T source) {
        this.source = source;
        fightType = FightType.CRUSH;
    }

    /**
     * Ticks the combat cooldown for the next attack. The combat strategy will
     * determine if the next attack will be performed in this tick.
     */
    public void tick() {
        if (cooldown > 0) {
            cooldown--;
        }

        Hit hit = hitQueue.poll();
        while (hit != null) {
            source.damage(hit);
            hit = hitQueue.poll();
        }

        if (strategy != null && target != null && strategy.canExecute(source, target)) {
            strategy.execute(source, target);
        }

        if (lastAttacker != null && elapsed.elapsed(CombatConstants.COMBAT_TIMEOUT)) {
            lastAttacker = null;
        }
    }

    public void queueHit(Hit hit) {
        hitQueue.add(hit);
    }

    /**
     * Targets an target to attack.
     *
     * @param target the targeted actor
     */
    public void target(Actor target) {
        source.face(target);
        source.interact(this.target);
        this.target = target;
    }

    public boolean canTarget(Actor actor) {
        return lastAttacker == null || lastAttacker.equals(actor);
    }

    /**
     * Resets the current combat session. Following, interaction facing, and the
     * current target get reset <strong>only</strong> if the current target is
     * not {@code null}.
     */
    public void reset() {
        if (target == null) {
            return;
        }

        target = null;
        source.resetFace();
    }

    /**
     * Sets the current {@code cooldown} to {@code delay}.
     *
     * @param delay the delay to add
     */
    public void cooldown(int delay) {
        if (cooldown < delay) {
            cooldown = delay;
        }
    }

    /**
     * Sets the current combat strategy used for attacks.
     *
     * @param strategy the strategy to set
     */
    public void setStrategy(CombatStrategy<? super T> strategy) {
        if (this.strategy == strategy) {
            return;
        }

        this.strategy = strategy;
    }

    public void setFightStyle(FightStyle fightStyle) {
        this.fightStyle = fightStyle;
    }

    public void setFightType(FightType fightType) {
        this.fightType = fightType;
    }

    public CombatRollMap<? super T> getFormulaModifiers() {
        return formulaModifiers;
    }

    public void addModifier(RollModifier<? super T> modifier) {
        formulaModifiers.add(modifier);
    }

    public void removeModifier(RollModifier<? super T> modifier) {
        formulaModifiers.remove(modifier);
    }

    /**
     * Sets the last attacker to {@code attacker} and resets the {@code elapsed}
     * time in combat.
     *
     * @param attacker the last attacker
     */
    public void setLastAttacker(Actor attacker) {
        lastAttacker = attacker;
        elapsed.reset();
    }

    /**
     * Checks if this {@code source} actor is currently attacking the {@code
     * other} actor.
     *
     * @param other the other actor
     * @return {@code true} if the other actor is currently being attacked by this
     * actor
     */
    public boolean isAttacking(Actor other) {
        return Objects.equals(target, other);
    }

    /**
     * Checks if this {@code actor} is currently under attack by the {@code other}
     * actor.
     *
     * @param other the other actor
     * @return {@code true} if the other actor is currently attacking this source
     * actor
     */
    public boolean isUnderAttackBy(Actor other) {
        return Objects.equals(lastAttacker, other);
    }

    /**
     * @return {@code true} if {@code elapsed} time exceeds {@link
     * CombatConstants#LOGOUT_TIMEOUT}
     */
    public boolean canLogout() {
        return elapsed.elapsed(CombatConstants.LOGOUT_TIMEOUT);
    }

    /** @return the milliseconds elapsed since last blocking a hit */
    public long elapsedTime() {
        return elapsed.elapsedTime();
    }

    /** @return the current combat cooldown */
    public int cooldown() {
        return cooldown;
    }

    /***
     * Sets the retaliation flag to incoming attacks
     * @param retaliate whether or not to retaliate
     */
    public void setRetaliate(boolean retaliate) {
        this.retaliate = retaliate;
    }

    /** @return {@code true} if retaliation is toggled */
    public boolean isRetaliate() {
        return retaliate;
    }

    /** @return the current {@code fightStyle} */
    public FightStyle getFightStyle() {
        return fightStyle;
    }

    /** @return the current {@code fightType} */
    public FightType getFightType() {
        return fightType;
    }

    /** @return the current {@code target} */
    public Actor getTarget() {
        return target;
    }

    /** @return the last attacker */
    public Actor getLastAttacker() {
        return lastAttacker;
    }

    public CombatType getCombatType() {
        if (strategy == null) {
            return CombatType.MELEE;
        }

        return strategy.getCombatType();
    }

    public int getAttackLevel(Actor defender) {
        int level = source.skills.getLevel(Skill.ATTACK);
        return formulaModifiers.compileOffensive(CombatRoll.ATTACK, source, defender, level);
    }

    public int getStrengthLevel(Actor defender) {
        int level = source.skills.getLevel(Skill.STRENGTH);
        return formulaModifiers.compileOffensive(CombatRoll.STRENGTH, source, defender, level);
    }

    public int getDefenceLevel(Actor attacker) {
        int level = source.skills.getLevel(Skill.DEFENCE);
        return formulaModifiers.compileDefensive(CombatRoll.DEFENCE, attacker, source, level);
    }

    public int getRangedLevel(Actor defender) {
        int level = source.skills.getLevel(Skill.RANGED);
        return formulaModifiers.compileOffensive(CombatRoll.RANGED, source, defender, level);
    }

    public int getMagicLevel(Actor defender) {
        int level = source.skills.getLevel(Skill.MAGIC);
        return formulaModifiers.compileOffensive(CombatRoll.MAGIC, source, defender, level);
    }

    public int getOffensiveRoll(Actor defender, int roll) {
        return formulaModifiers.compileOffensive(CombatRoll.OFFENSIVE, source, defender, roll);
    }

    public int getAggresiveRoll(Actor defender, int roll) {
        return formulaModifiers.compileOffensive(CombatRoll.AGGRESSIVE, source, defender, roll);
    }

    public int getDefensiveRoll(Actor attacker, int roll) {
        return formulaModifiers.compileDefensive(CombatRoll.DEFENSIVE, attacker, source, roll);
    }


    /******* For debug commands *******/

    public int getAttackLevel(Actor defender, int level) {
        return formulaModifiers.compileOffensive(CombatRoll.ATTACK, source, defender, level);
    }

    public int getStrengthLevel(Actor defender, int level) {
        return formulaModifiers.compileOffensive(CombatRoll.STRENGTH, source, defender, level);
    }

    public int getDefenceLevel(Actor attacker, int level) {
        return formulaModifiers.compileDefensive(CombatRoll.DEFENCE, attacker, source, level);
    }

    public int getRangedLevel(Actor defender, int level) {
        return formulaModifiers.compileOffensive(CombatRoll.RANGED, source, defender, level);
    }

    public int getMagicLevel(Actor defender, int level) {
        return formulaModifiers.compileOffensive(CombatRoll.MAGIC, source, defender, level);
    }

    public int getAttackRadius() {
        return strategy == null ? 1 : strategy.getAttackDistance(source);
    }

}
