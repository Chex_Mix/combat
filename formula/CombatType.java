package com.osroyale.game.world.entity.combat.formula;

import com.osroyale.game.world.entity.combat.formula.impl.MagicFormula;
import com.osroyale.game.world.entity.combat.formula.impl.MeleeFormula;
import com.osroyale.game.world.entity.combat.formula.impl.RangedFormula;

public enum CombatType {
    MELEE(new MeleeFormula()),
    RANGED(new RangedFormula()),
    MAGIC(new MagicFormula());

    private final CombatFormula formula;

    CombatType(CombatFormula formula) {
        this.formula = formula;
    }

    public CombatFormula getFormula() {
        return formula;
    }

}
