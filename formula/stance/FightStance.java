package com.osroyale.game.world.entity.combat.formula.stance;

public class FightStance {
    private final int button;
    private final int parentConfig;
    private final int childConfig;
    private final FightType fightType;
    private final FightStyle fightStyle;

    public FightStance(int button, int parentConfig, int childConfig, FightType fightType, FightStyle fightStyle) {
        this.button = button;
        this.parentConfig = parentConfig;
        this.childConfig = childConfig;
        this.fightType = fightType;
        this.fightStyle = fightStyle;
    }

    public int getButton() {
        return button;
    }

    public int getParentConfig() {
        return parentConfig;
    }

    public int getChildConfig() {
        return childConfig;
    }

    public FightType getFightType() {
        return fightType;
    }

    public FightStyle getFightStyle() {
        return fightStyle;
    }

}
