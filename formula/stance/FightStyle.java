package com.osroyale.game.world.entity.combat.formula.stance;

/**
 * The enumerated type whose elements represent the fighting styles.
 *
 * @author Michael | Chex
 */
public enum FightStyle {
    ACCURATE(3, 0, 0),
    AGGRESSIVE(0, 3, 0),
    DEFENSIVE(0, 0, 3),
    CONTROLLED(1, 1, 1),
    EMPTY(0, 0, 0);

    /** The offensive increase. */
    private int offensiveIncrease;

    /** The aggressive increase. */
    private int aggressiveIncrease;

    /** The defensive increase. */
    private int defensiveIncrease;

    /** Constructs a new {@link FightStyle} element. */
    FightStyle(int offensiveIncrease, int aggressiveIncrease, int defensiveIncrease) {
        this.offensiveIncrease = offensiveIncrease;
        this.aggressiveIncrease = aggressiveIncrease;
        this.defensiveIncrease = defensiveIncrease;
    }

    /** @return the offensive increase */
    public int getOffensiveIncrease() {
        return offensiveIncrease;
    }

    /** @return the aggressive increase */
    public int getAggressiveIncrease() {
        return aggressiveIncrease;
    }

    /** @return the defensive increase */
    public int getDefensiveIncrease() {
        return defensiveIncrease;
    }

}