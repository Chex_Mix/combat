package com.osroyale.game.world.entity.combat.formula.stance;

import com.osroyale.game.world.items.containers.equipment.Equipment;

/**
 * The enumerated type whose elements represent the fighting types.
 *
 * @author Michael | Chex
 */
public enum FightType {
    STAB(Equipment.STAB_OFFENSE, Equipment.STAB_DEFENSE),
    SLASH(Equipment.SLASH_OFFENSE, Equipment.SLASH_DEFENSE),
    CRUSH(Equipment.CRUSH_OFFENSE, Equipment.CRUSH_DEFENCE),
    RANGED(Equipment.RANGED_OFFENSE, Equipment.RANGED_DEFENSE),
    MAGIC(Equipment.MAGIC_OFFENSE, Equipment.MAGIC_DEFENSE);

    /** The offensive equipment bonus to target. */
    private final int offensiveBonus;

    /** The defensive equipment bonus to target. */
    private final int defensiveBonus;

    /** Constructs a new {@link FightType} element. */
    FightType(int offensiveBonus, int defensiveBonus) {
        this.offensiveBonus = offensiveBonus;
        this.defensiveBonus = defensiveBonus;
    }

    /** @return the offensive equipment bonus */
    public int getOffensiveBonus() {
        return offensiveBonus;
    }

    /** @return the defensive equipment bonus */
    public int getDefensiveBonus() {
        return defensiveBonus;
    }

}