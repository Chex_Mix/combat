package com.osroyale.game.world.entity.combat.formula;

import com.osroyale.game.world.entity.actor.Actor;

public interface CombatFormula {

    int getOffensiveRoll(Actor attacker, Actor defender);

    int getAggressiveRoll(Actor attacker, Actor defender);

    int getDefensiveRoll(Actor attacker, Actor defender);

    int getOffensiveBonus(Actor attacker, Actor defender);

    int getAggressiveBonus(Actor attacker, Actor defender);

    int getDefensiveBonus(Actor attacker, Actor defender);

}
