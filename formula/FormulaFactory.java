package com.osroyale.game.world.entity.combat.formula;

import com.osroyale.game.world.entity.actor.Actor;
import com.osroyale.game.world.entity.combat.hit.Hit;
import com.osroyale.game.world.entity.combat.hit.HitIcon;
import com.osroyale.game.world.entity.combat.hit.Hitsplat;
import com.osroyale.util.RandomUtils;

/**
 * Supplies factory methods useful for combat.
 *
 * @author Michael | Chex
 */
public final class FormulaFactory {

    public static Hit nextMeleeHit(int max) {
        return nextHit(max, Hitsplat.NORMAL, HitIcon.MELEE);
    }

    public static Hit nextRangedHit(int max) {
        return nextHit(max, Hitsplat.NORMAL, HitIcon.RANGED);
    }

    public static Hit nextMagicHit(int max) {
        return nextHit(max, Hitsplat.NORMAL, HitIcon.MAGIC);
    }

    public static Hit nextPoisonHit(int max) {
        return nextHit(max, Hitsplat.POISON, HitIcon.NONE);
    }

    public static Hit nextVenomHit(int max) {
        return nextHit(max, Hitsplat.VENOM, HitIcon.NONE);
    }

    public static Hit nextHit(int max) {
        return nextHit(max, Hitsplat.NORMAL, HitIcon.NONE);
    }

    public static Hit nextMeleeHit(Actor attacker, Actor defender, CombatType base, int max) {
        return nextHit(attacker, defender, base.getFormula(), max, Hitsplat.NORMAL, HitIcon.MELEE);
    }

    public static Hit nextRangedHit(Actor attacker, Actor defender, CombatType base, int max) {
        return nextHit(attacker, defender, base.getFormula(), max, Hitsplat.NORMAL, HitIcon.RANGED);
    }

    public static Hit nextMagicHit(Actor attacker, Actor defender, CombatType base, int max) {
        return nextHit(attacker, defender, base.getFormula(), max, Hitsplat.NORMAL, HitIcon.MAGIC);
    }

    public static Hit nextPoisonHit(Actor attacker, Actor defender, CombatType base, int max) {
        return nextHit(attacker, defender, base.getFormula(), max, Hitsplat.POISON, HitIcon.NONE);
    }

    public static Hit nextVenomHit(Actor attacker, Actor defender, CombatType base, int max) {
        return nextHit(attacker, defender, base.getFormula(), max, Hitsplat.VENOM, HitIcon.NONE);
    }

    public static Hit nextHit(Actor attacker, Actor defender, CombatType base, int max) {
        return nextHit(attacker, defender, base.getFormula(), max, Hitsplat.NORMAL, HitIcon.NONE);
    }

    private static Hit nextHit(Actor attacker, Actor defender, CombatFormula formula, int max, Hitsplat hitsplat, HitIcon icon) {
        int attackRoll = rollOffensive(attacker, defender, formula);
        int defenceRoll = rollDefensive(attacker, defender, formula);

        if (!accurate(attackRoll, defenceRoll)) {
            return new Hit(0, hitsplat, icon, false);
        }

        return nextHit(max, hitsplat, icon);
    }

    private static Hit nextHit(int max, Hitsplat hitsplat, HitIcon icon) {
        int damage = RandomUtils.inclusive(0, max);
        return new Hit(damage, hitsplat, icon, true);
    }

    private static boolean accurate(int attackRoll, int defenceRoll) {
        return RandomUtils.success(attackRoll / (double) (attackRoll + defenceRoll));
    }

    public static int rollOffensive(Actor attacker, Actor defender, CombatFormula formula) {
        int roll = formula.getOffensiveRoll(attacker, defender);
        int bonus = formula.getOffensiveBonus(attacker, defender);
        roll = attacker.getCombat().getOffensiveRoll(defender, roll);
        return getRoll(roll, bonus);
    }

    public static int rollDefensive(Actor attacker, Actor defender, CombatFormula formula) {
        int roll = formula.getDefensiveRoll(attacker, defender);
        int bonus = formula.getDefensiveBonus(attacker, defender);
        roll = defender.getCombat().getDefensiveRoll(attacker, roll);
        return getRoll(roll, bonus);
    }

    public static int getMaxHit(Actor attacker, Actor defender, CombatFormula formula) {
        int maxHit = formula.getAggressiveRoll(attacker, defender);
        int bonus = formula.getAggressiveBonus(attacker, defender);
        maxHit = attacker.getCombat().getAggresiveRoll(defender, maxHit);
        return maxHit(maxHit, bonus);
    }

    private static int maxHit(int level, int bonus) {
        return (320 + getRoll(level - 8, bonus)) / 640;
    }

    private static int getRoll(int level, int bonus) {
        return (level + 8) * (bonus + 64);
    }

}
