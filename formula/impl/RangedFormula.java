package com.osroyale.game.world.entity.combat.formula.impl;

import com.osroyale.game.world.entity.actor.Actor;
import com.osroyale.game.world.entity.combat.formula.stance.FightStyle;
import com.osroyale.game.world.entity.combat.formula.CombatFormula;
import com.osroyale.game.world.items.containers.equipment.Equipment;

public class RangedFormula implements CombatFormula {

    @Override
    public int getOffensiveRoll(Actor attacker, Actor defender) {
        FightStyle style = attacker.getCombat().getFightStyle();
        int level = attacker.getCombat().getRangedLevel(defender);
        return level + style.getOffensiveIncrease();
    }

    @Override
    public int getAggressiveRoll(Actor attacker, Actor defender) {
        FightStyle style = attacker.getCombat().getFightStyle();
        int level = attacker.getCombat().getRangedLevel(defender);
        return level + style.getAggressiveIncrease();
    }

    @Override
    public int getDefensiveRoll(Actor attacker, Actor defender) {
        FightStyle style = attacker.getCombat().getFightStyle();
        int level = defender.getCombat().getDefenceLevel(attacker);
        return level + style.getDefensiveIncrease();
    }

    @Override
    public int getOffensiveBonus(Actor attacker, Actor defender) {
        return attacker.getBonus(Equipment.RANGED_OFFENSE);
    }

    @Override
    public int getAggressiveBonus(Actor attacker, Actor defender) {
        return attacker.getBonus(Equipment.RANGED_STRENGTH);
    }

    @Override
    public int getDefensiveBonus(Actor attacker, Actor defender) {
        return defender.getBonus(Equipment.RANGED_DEFENSE);
    }

}
