package com.osroyale.game.world.entity.combat.formula.impl;

import com.osroyale.game.world.entity.actor.Actor;
import com.osroyale.game.world.entity.combat.formula.stance.FightStyle;
import com.osroyale.game.world.entity.combat.formula.CombatFormula;
import com.osroyale.game.world.items.containers.equipment.Equipment;

public class MagicFormula implements CombatFormula {

    @Override
    public int getOffensiveRoll(Actor attacker, Actor defender) {
        return attacker.getCombat().getMagicLevel(defender);
    }

    @Override
    public int getAggressiveRoll(Actor attacker, Actor defender) {
        return attacker.getCombat().getMagicLevel(defender);
    }

    @Override
    public int getDefensiveRoll(Actor attacker, Actor defender) {
        FightStyle style = defender.getCombat().getFightStyle();

        int magic = defender.getCombat().getMagicLevel(attacker);
        int defence = defender.getCombat().getDefenceLevel(attacker);

        int effectiveLevel = (7 * magic + 3 * defence) / 10;
        return effectiveLevel + style.getDefensiveIncrease();
    }

    @Override
    public int getOffensiveBonus(Actor attacker, Actor defender) {
        return attacker.getBonus(Equipment.MAGIC_OFFENSE);
    }

    @Override
    public int getAggressiveBonus(Actor attacker, Actor defender) {
        return attacker.getBonus(Equipment.MAGIC_STRENGTH);
    }

    @Override
    public int getDefensiveBonus(Actor attacker, Actor defender) {
        return defender.getBonus(Equipment.MAGIC_DEFENSE);
    }

}
