package com.osroyale.game.world.entity.combat.formula.modifier;

import com.osroyale.game.world.entity.actor.Actor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public final class CombatRollMap<T extends Actor> {
    private final ArrayList<Set<RollModifier<? super T>>> rolls;

    public CombatRollMap() {
        rolls = new ArrayList<>(CombatRoll.values().length);

        for (CombatRoll roll : CombatRoll.values()) {
            rolls.add(roll.ordinal(), new HashSet<>());
        }
    }

    public void add(RollModifier<? super T> modifier) {
        Set<RollModifier<? super T>> set = rolls.get(modifier.getRoll().ordinal());

        if (set == null) {
            rolls.add(modifier.getRoll().ordinal(), set = new HashSet<>());
        }

        set.add(modifier);
    }

    public void remove(RollModifier<? super T> modifier) {
        Set<RollModifier<? super T>> set = rolls.get(modifier.getRoll().ordinal());

        if (set == null) {
            return;
        }

        set.remove(modifier);
    }

    public int compileOffensive(CombatRoll roll, T attacker, Actor defender, int base) {
        Set<RollModifier<? super T>> set = rolls.get(roll.ordinal());

        if (set == null) {
            return base;
        }

        for (RollModifier<? super T> modifier : set) {
            base = modifier.modifyOffensive(attacker, defender, base);
        }

        return base;
    }

    public int compileDefensive(CombatRoll roll, Actor attacker, T defender, int base) {
        Set<RollModifier<? super T>> set = rolls.get(roll.ordinal());

        if (set == null) {
            return base;
        }

        for (RollModifier<? super T> modifier : set) {
            base = modifier.modifyDefensive(attacker, defender, base);
        }

        return base;
    }

}
