package com.osroyale.game.world.entity.combat.formula.modifier;

public enum CombatRoll {
    ATTACK,
    STRENGTH,
    DEFENCE,
    RANGED,
    MAGIC,

    OFFENSIVE,
    AGGRESSIVE,
    DEFENSIVE,

    DAMAGE

}
