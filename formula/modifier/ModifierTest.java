package com.osroyale.game.world.entity.combat.formula.modifier;

import com.osroyale.game.world.entity.actor.Actor;

public class ModifierTest {

    public static void main(String[] args) {
        CombatRollMap<Actor> map = new CombatRollMap<>();

        RollModifier<Actor> base = new RollModifier<Actor>() {
            @Override
            public int modifyOffensive(Actor attacker, Actor defender, int roll) {
                return roll * 11 / 10;
            }

            @Override
            public CombatRoll getRoll() {
                return CombatRoll.ATTACK;
            }
        };

        RollModifier<Actor> modifier = new RollModifier<Actor>() {
            @Override
            public int modifyOffensive(Actor attacker, Actor defender, int roll) {
                return roll * 5 / 4;
            }

            @Override
            public CombatRoll getRoll() {
                return CombatRoll.ATTACK;
            }
        };

        map.add(base);
        map.add(modifier);
        System.out.println(map.compileOffensive(CombatRoll.ATTACK, null, null, 1000));

        map.remove(base);
        map.remove(modifier);
        System.out.println(map.compileOffensive(CombatRoll.ATTACK, null, null, 1000));
    }

}
