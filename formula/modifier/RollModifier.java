package com.osroyale.game.world.entity.combat.formula.modifier;

import com.osroyale.game.world.entity.actor.Actor;

public interface RollModifier<T extends Actor> {

    default int modifyOffensive(T attacker, Actor defender, int roll) {
        return roll;
    }

    default int modifyDefensive(Actor attacker, T defender, int roll) {
        return roll;
    }

    CombatRoll getRoll();

}
