package com.osroyale.game.world.entity.combat.strategy;

import com.osroyale.game.world.entity.actor.Actor;
import com.osroyale.game.world.entity.combat.formula.CombatType;
import com.osroyale.game.world.entity.combat.hit.Hit;

public interface CombatStrategy<T extends Actor> {

    boolean canExecute(T attacker, Actor defender);

    void execute(T attacker, Actor defender);

    void start(T attacker, Actor defender, Hit[] hits);

    void hitsplat(T attacker, Actor defender, Hit hit);

    int getAttackDistance(T attacker);

    CombatType getCombatType();

}
