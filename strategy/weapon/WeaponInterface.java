package com.osroyale.game.world.entity.combat.strategy.weapon;
//
//import com.osroyale.Config;
//import com.osroyale.game.world.entity.combat.formula.stance.FightStyle;
//import com.osroyale.game.world.entity.combat.formula.stance.FightType;
//import com.osroyale.game.world.entity.actor.player.Player;
//import com.osroyale.game.world.items.Item;
//import com.osroyale.net.packet.out.SendConfig;
//import com.osroyale.net.packet.out.SendString;
//
//import java.util.Optional;
//
public enum WeaponInterface {

    /*

    328   - Magic Staff
    425   - Hammer
    5855  - default
    6103  - staff

    12290 - whip

    776   - scythe
    1698  - axe
    2276  - short sword
    4705  - 2h sword
    5570  - pick axe

    2423  - longsword
    3796  - mace
    7762  - claws

    4679  - spear

    8460  - halberd

    1764  - bow
    4446  - thrown
    24055 - chinchompa

     */

    UNARMED
}
//    UNARMED(5855, 5857, 7737, 7749, 7761, new StyleType[] {
//        new StyleType(FightStyle.ACCURATE, FightType.CRUSH),
//        new StyleType(FightStyle.AGGRESSIVE, FightType.CRUSH),
//        new StyleType(FightStyle.DEFENSIVE, FightType.CRUSH)
//    }),
//
//    DART(4446, 4449, 7637, 7649, 7661, new StyleType[] {
//        new StyleType(FightStyle.ACCURATE, FightType.RANGED),
//        new StyleType(FightStyle.EMPTY, FightType.RANGED),
//        new StyleType(FightStyle.DEFENSIVE, FightType.RANGED)
//    }),
//
//    STAFF(6103, 6132, 6104, 6117, 6129, new StyleType[] {
//        new StyleType(FightStyle.ACCURATE, FightType.CRUSH),
//        new StyleType(FightStyle.AGGRESSIVE, FightType.CRUSH),
//        new StyleType(FightStyle.DEFENSIVE, FightType.CRUSH)
//    }),
//
//    MAGIC_STAFF(328, 355, 340, 18566, 18569, new StyleType[] {
//        new StyleType(FightStyle.ACCURATE, FightType.CRUSH),
//        new StyleType(FightStyle.AGGRESSIVE, FightType.CRUSH),
//        new StyleType(FightStyle.DEFENSIVE, FightType.CRUSH)
//    }),
//
//    WARHAMMER(425, 428, 7473, 7474, 7486, new StyleType[] {
//        new StyleType(FightStyle.ACCURATE, FightType.CRUSH),
//        new StyleType(FightStyle.AGGRESSIVE, FightType.CRUSH),
//        new StyleType(FightStyle.DEFENSIVE, FightType.CRUSH)
//    }),
//
//    SCYTHE(776, 779, -1, -1, -1, new StyleType[] {
//        new StyleType(FightStyle.ACCURATE, FightType.SLASH),
//        new StyleType(FightStyle.AGGRESSIVE, FightType.STAB),
//        new StyleType(FightStyle.AGGRESSIVE, FightType.CRUSH),
//        new StyleType(FightStyle.DEFENSIVE, FightType.SLASH)
//    }),
//
//    BATTLEAXE(1698, 1701, 7498, 7499, 7511, new StyleType[] {
//        new StyleType(FightStyle.ACCURATE, FightType.SLASH),
//        new StyleType(FightStyle.AGGRESSIVE, FightType.SLASH),
//        new StyleType(FightStyle.AGGRESSIVE, FightType.CRUSH),
//        new StyleType(FightStyle.DEFENSIVE, FightType.SLASH)
//    }),
//
//    CROSSBOW(1764, 1767, 7548, 7549, 7561, new StyleType[] {
//        new StyleType(FightStyle.ACCURATE, FightType.RANGED),
//        new StyleType(FightStyle.EMPTY, FightType.RANGED),
//        new StyleType(FightStyle.DEFENSIVE, FightType.RANGED)
//    }),
//
//    SHORTBOW(1764, 1767, 7548, 7549, 7561, new StyleType[] {
//        new StyleType(FightStyle.ACCURATE, FightType.RANGED),
//        new StyleType(FightStyle.EMPTY, FightType.RANGED),
//        new StyleType(FightStyle.DEFENSIVE, FightType.RANGED)
//    }),
//
//    LONGBOW(1764, 1767, 7548, 7549, 7561, new StyleType[] {
//        new StyleType(FightStyle.ACCURATE, FightType.RANGED),
//        new StyleType(FightStyle.EMPTY, FightType.RANGED),
//        new StyleType(FightStyle.DEFENSIVE, FightType.RANGED)
//    }),
//
//    DARK_BOW(1764, 1767, 7548, 7549, 7561, new StyleType[] {
//        new StyleType(FightStyle.ACCURATE, FightType.RANGED),
//        new StyleType(FightStyle.EMPTY, FightType.RANGED),
//        new StyleType(FightStyle.DEFENSIVE, FightType.RANGED)
//    }),
//
//    COMPOSITE_BOW(1764, 1767, 7548, 7549, 7561, new StyleType[] {
//        new StyleType(FightStyle.ACCURATE, FightType.RANGED),
//        new StyleType(FightStyle.EMPTY, FightType.RANGED),
//        new StyleType(FightStyle.DEFENSIVE, FightType.RANGED)
//    }),
//
//    DAGGER(2276, 2279, 7562, 7574, 7586, new StyleType[] {
//        new StyleType(FightStyle.ACCURATE, FightType.STAB),
//        new StyleType(FightStyle.AGGRESSIVE, FightType.STAB),
//        new StyleType(FightStyle.AGGRESSIVE, FightType.SLASH),
//        new StyleType(FightStyle.DEFENSIVE, FightType.STAB)
//    }),
//
//    SWORD(2276, 2279, 7562, 7574, 7586, new StyleType[] {
//        new StyleType(FightStyle.ACCURATE, FightType.STAB),
//        new StyleType(FightStyle.AGGRESSIVE, FightType.STAB),
//        new StyleType(FightStyle.AGGRESSIVE, FightType.SLASH),
//        new StyleType(FightStyle.DEFENSIVE, FightType.STAB)
//    }),
//
//    LONGSWORD(2423, 2426, 7587, 7599, 7611, new StyleType[] {
//        new StyleType(FightStyle.ACCURATE, FightType.SLASH),
//        new StyleType(FightStyle.AGGRESSIVE, FightType.SLASH),
//        new StyleType(FightStyle.CONTROLLED, FightType.STAB),
//        new StyleType(FightStyle.DEFENSIVE, FightType.SLASH)
//    }),
//
//    SCIMITAR(2423, 2426, 7587, 7599, 7611, new StyleType[] {
//        new StyleType(FightStyle.ACCURATE, FightType.SLASH),
//        new StyleType(FightStyle.AGGRESSIVE, FightType.SLASH),
//        new StyleType(FightStyle.CONTROLLED, FightType.STAB),
//        new StyleType(FightStyle.DEFENSIVE, FightType.SLASH)
//    }),
//
//    MACE(3796, 3799, 7623, 7624, 7636, new StyleType[] {
//        new StyleType(FightStyle.ACCURATE, FightType.CRUSH),
//        new StyleType(FightStyle.AGGRESSIVE, FightType.CRUSH),
//        new StyleType(FightStyle.CONTROLLED, FightType.STAB),
//        new StyleType(FightStyle.DEFENSIVE, FightType.CRUSH)
//    }),
//
//    KNIFE(4446, 4449, 7637, 7649, 7661, new StyleType[] {
//        new StyleType(FightStyle.ACCURATE, FightType.RANGED),
//        new StyleType(FightStyle.EMPTY, FightType.RANGED),
//        new StyleType(FightStyle.DEFENSIVE, FightType.RANGED)
//    }),
//
//    SPEAR(4679, 4682, 7662, 7674, 7686, new StyleType[] {
//        new StyleType(FightStyle.CONTROLLED, FightType.STAB),
//        new StyleType(FightStyle.CONTROLLED, FightType.SLASH),
//        new StyleType(FightStyle.CONTROLLED, FightType.CRUSH),
//        new StyleType(FightStyle.DEFENSIVE, FightType.STAB)
//    }),
//
//    TWO_HANDED_SWORD(4705, 4708, 7687, 7699, 7711, new StyleType[] {
//        new StyleType(FightStyle.CONTROLLED, FightType.STAB),
//        new StyleType(FightStyle.CONTROLLED, FightType.SLASH),
//        new StyleType(FightStyle.CONTROLLED, FightType.CRUSH),
//        new StyleType(FightStyle.DEFENSIVE, FightType.STAB)
//    }),
//
//    PICKAXE(5570, 5573, 7723, 7724, 7736, new StyleType[] {
//            FightType.PICKAXE_SPIKE,
//            FightType.PICKAXE_IMPALE,
//            FightType.PICKAXE_SMASH,
//            FightType.PICKAXE_BLOCK
//    }),
//
//    CLAWS(7762, 7765, 7788, 7800, 7812, new StyleType[] {
//            FightType.CLAWS_CHOP,
//            FightType.CLAWS_SLASH,
//            FightType.CLAWS_LUNGE,
//            FightType.CLAWS_BLOCK
//    }),
//
//    HALBERD(8460, 8463, 8481, 8493, 8505, new StyleType[] {
//            FightType.HALBERD_JAB,
//            FightType.HALBERD_SWIPE,
//            FightType.HALBERD_FEND
//    }),
//
//    WHIP(12290, 12293, 12322, 12323, 12335, new StyleType[] {
//            FightType.WHIP_FLICK,
//            FightType.WHIP_LASH,
//            FightType.WHIP_DEFLECT
//    }),
//
//    THROWNAXE(4446, 4449, 7637, 7649, 7661, new StyleType[] {
//            FightType.THROWNAXE_ACCURATE,
//            FightType.THROWNAXE_RAPID,
//            FightType.THROWNAXE_LONGRANGE
//    }),
//
//    CHINCHOMPA(24055, 24056, -1, -1, -1, new StyleType[] {
//            FightType.SHORT_FUSE,
//            FightType.MEDIUM_FUSE,
//            FightType.LONG_FUSE
//    }),
//
//    SALAMANDER(24074, 24075, -1, -1, -1, new StyleType[] {
//            FightType.SCORCH,
//            FightType.FLARE,
//            FightType.BLAZE
//    }),
//
//    GRANITE_MAUL(425, 428, 7473, 7474, 7486, new StyleType[] {
//            FightType.GRANITE_MAUL_POUND,
//            FightType.GRANITE_MAUL_PUMMEL,
//            FightType.GRANITE_MAUL_BLOCK
//    }),
//
//    GREATAXE(1698, 1701, 7498, 7499, 7591, new StyleType[] {
//            FightType.GREATAXE_CHOP,
//            FightType.GREATAXE_HACK,
//            FightType.GREATAXE_SMASH,
//            FightType.GREATAXE_BLOCK
//    }),
//
//    KARIL_CROSSBOW(1764, 1767, 7548, 7549, 7561, new StyleType[] {
//            FightType.KARIL_CROSSBOW_ACCURATE,
//            FightType.KARIL_CROSSBOW_RAPID,
//            FightType.KARIL_CROSSBOW_LONGRANGE
//    }),
//
//    BALLISTA(1764, 1767, 7548, 7549, 7561, new StyleType[] {
//            FightType.BALLISTA_ACCURATE,
//            FightType.BALLISTA_RAPID,
//            FightType.BALLISTA_LONGRANGE
//    }),
//
//    DRAGON_DAGGER(2276, 2279, 7562, 7574, 7586, new StyleType[] {
//            FightType.DRAGON_DAGGER_STAB,
//            FightType.DRAGON_DAGGER_LUNGE,
//            FightType.DRAGON_DAGGER_SLASH,
//            FightType.DRAGON_DAGGER_BLOCK
//    }),
//
//    FLAIL(3796, 3799, 7623, 7624, 7636, new StyleType[] {
//            FightType.FLAIL_POUND,
//            FightType.FLAIL_PUMMEL,
//            FightType.FLAIL_SPIKE,
//            FightType.FLAIL_BLOCK
//    }),
//
//    GODSWORD_SWORD(4705, 4708, 7687, 7699, 7711, new StyleType[] {
//            FightType.GODSWORD_CHOP,
//            FightType.GODSWORD_SLASH,
//            FightType.GODSWORD_SMASH,
//            FightType.GODSWORD_BLOCK
//    }),
//
//    SARADOMIN_SWORD(4705, 4708, 7687, 7699, 7711, new StyleType[] {
//            FightType.SARADOMIN_CHOP,
//            FightType.SARADOMIN_SLASH,
//            FightType.SARADOMIN_SMASH,
//            FightType.SARADOMIN_BLOCK
//    }),
//
//    TRIDENT(4446, 4449, 7637, 7649, 7661, new StyleType[] {
//            FightType.TRIDENT_ACCURATE,
//            FightType.TRIDENT_RAPID,
//            FightType.TRIDENT_LONGRANGE
//    }),
//
//    BLOWPIPE(4446, 4449, 7637, 7649, 7661, new StyleType[] {
//            FightType.BLOWPIPE_ACCURATE,
//            FightType.BLOWPIPE_RAPID,
//            FightType.BLOWPIPE_LONGRANGE
//    }),
//
//    ELDER_MAUL(425, 428, 7473, 7474, 7486, new StyleType[] {
//            FightType.ELDER_MAUL_POUND,
//            FightType.ELDER_MAUL_PUMMEL,
//            FightType.ELDER_MAUL_BLOCK
//    }),
//
//    BULWARK(425, 428, 7473, 7474, 7486, new StyleType[] {
//            FightType.BULWARK_POUND,
//            FightType.BULWARK_PUMMEL,
//            FightType.BULWARK_BLOCK
//    }),
//
//    BLUDGEN(4705, 4708, 7687, 7699, 7711, new StyleType[] {
//            FightType.BLUDGEN_CHOP,
//            FightType.BLUDGEN_SLASH,
//            FightType.BLUDGEN_SMASH,
//            FightType.BLUDGEN_BLOCK
//    });
//
//    /**
//     * The identification of the interface that will be displayed.
//     */
//    private final int id;
//
//    /**
//     * The identification of the line the weapon name will be displayed on.
//     */
//    private final int nameLine;
//
//    /**
//     * The style types that correspond with this interface.
//     */
//    private final StyleType[] fightTypes;
//
//    /**
//     * The identification of the special bar for this interface.
//     */
//    private final int specialButton;
//
//    /**
//     * The identification of the special bar for this interface.
//     */
//    private final int specialBar;
//
//    /**
//     * The identification of the special meter for this interface.
//     */
//    private final int specialMeter;
//
//    /**
//     * Creates a new {@link WeaponInterface}.
//     *
//     * @param id         the identification of the interface that will be
//     *                   displayed.
//     * @param nameLine   the identification of the line the weapon name will
//     *                   be displayed on.
//     * @param styleTypes the fight types that correspond with this interface.
//     */
//    WeaponInterface(int id, int nameLine, int specialButton, int specialBar, int specialMeter, StyleType[] styleTypes) {
//        this.id = id;
//        this.nameLine = nameLine;
//        this.fightTypes = styleTypes;
//        this.specialButton = specialButton;
//        this.specialBar = specialBar;
//        this.specialMeter = specialMeter;
//    }
//
//    /**
//     * The method executed when weapon {@code item} is equipped or unequipped
//     * that assigns a weapon interface to {@code player}.
//     *
//     * @param player the player equipping the item.
//     * @param item   the item the player is equipping, or {@code null} if a
//     *               weapon was unequipped.
//     */
//    public static void execute(Player player, Item item) {
//        WeaponInterface weapon = item == null ? null : item.getWeaponInterface();
//        if (weapon == null) weapon = WeaponInterface.UNARMED;
//
//        StyleType[] oldTypes = player.getWeapon().fightTypes;
//        StyleType[] newTypes = weapon.fightTypes;
//        StyleType result = null;
//
//        for (int index = 0; index < oldTypes.length; index++) {
//            if (newTypes.length == index) {
//                break;
//            }
//
//            if (newTypes[index].style.equals(FightStyle.DEFENSIVE)
//                    && player.getCombat().getFightStyle().equals(FightStyle.DEFENSIVE)) {
//                result = newTypes[index];
//                break;
//            }
//
//            if (oldTypes[index].isStyle(player.getCombat().getFightStyle())) {
//                boolean oldControlled = oldTypes[index].isStyle(FightStyle.CONTROLLED);
//                boolean newControlled = newTypes[index].isStyle(FightStyle.CONTROLLED);
//
//                if (newControlled != oldControlled) {
//                    continue;
//                }
//
//                result = newTypes[index];
//                break;
//            }
//        }
//
//        if (result == null) {
//            if (player.getCombat().getFightStyle().equals(FightStyle.DEFENSIVE)) {
//                result = newTypes[newTypes.length - 1];
//            } else {
//                result = newTypes[1];
//            }
//        }
//
//        player.setWeapon(weapon);
//        player.getCombat().setFightType(result);
//        player.send(new SendConfig(result.getParent(), result.getChild()));
//
//        if (weapon == WeaponInterface.UNARMED) {
//            defaultWeaponInterface(player);
//            return;
//        }
//
////        CombatSpecial.assign(player);
////        CombatSpecial.updateSpecialAmount(player);
////        Autocast.reset(player);
////        player.setSingleCast(null);
//
//        player.send(new SendString(item.getName(), weapon.nameLine));
//        player.interfaceManager.setSidebar(Config.ATTACK_TAB, weapon.id);
//        player.equipment.updateAnimation();
//    }
//
//    private static void defaultWeaponInterface(Player player) {
////        CombatSpecial.assign(player);
////        CombatSpecial.updateSpecialAmount(player);
////        Autocast.reset(player);
////        player.setSingleCast(null);
//
//        player.send(new SendString("<col=ff7000>Unarmed", WeaponInterface.UNARMED.nameLine));
//        player.interfaceManager.setSidebar(Config.ATTACK_TAB, WeaponInterface.UNARMED.id);
//        player.equipment.updateAnimation();
//    }
//
//    /**
//     * Gets the identification of the interface that will be displayed.
//     *
//     * @return the identification of the interface.
//     */
//    public final int getId() {
//        return id;
//    }
//
//    /**
//     * Gets the fight types that correspond with this interface.
//     *
//     * @return the fight types that correspond.
//     */
//    public final StyleType[] getFightTypes() {
//        return fightTypes;
//    }
//
//    /**
//     * Gets the identification of the special bar for this interface.
//     *
//     * @return the identification of the special bar.
//     */
//    public final int getSpecialBar() {
//        return specialBar;
//    }
//
//    /**
//     * Gets the identification of the special meter for this interface.
//     *
//     * @return the identification of the special meter.
//     */
//    public final int getSpecialMeter() {
//        return specialMeter;
//    }
//
//    public static boolean isSpecialButton(int button) {
//        for (WeaponInterface weaponInterface : values()) {
//            if (weaponInterface.specialButton == button) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    private static class StyleType {
//        private final FightStyle style;
//        private final FightType type;
//
//        private StyleType(FightStyle style, FightType type) {
//            this.style = style;
//            this.type = type;
//        }
//
//        public boolean isStyle(FightStyle other) {
//            return style.equals(other);
//        }
//
//        public boolean isType(FightType other) {
//            return type.equals(other);
//        }
//
//    }
//
//}
