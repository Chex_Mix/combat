package com.osroyale.game.world.entity.combat.strategy.weapon.sidebar;

import com.osroyale.game.world.entity.actor.player.Player;
import com.osroyale.game.world.entity.combat.formula.stance.FightStance;
import com.osroyale.game.world.entity.combat.formula.stance.FightStyle;
import com.osroyale.game.world.entity.combat.formula.stance.FightType;
import com.osroyale.game.world.entity.combat.strategy.weapon.WeaponSidebar;

public class DefaultSidebar extends WeaponSidebar {

    private static final FightStance[] stances = {
        new FightStance(5860, 43, 0, FightType.CRUSH, FightStyle.ACCURATE),
        new FightStance(5862, 43, 1, FightType.CRUSH, FightStyle.AGGRESSIVE),
        new FightStance(5861, 43, 2, FightType.CRUSH, FightStyle.DEFENSIVE),
    };

    public DefaultSidebar(Player player) {
        super(player);
    }

    @Override
    public int getInterfaceId() {
        return 5855;
    }

    @Override
    public int getWeaponTextId() {
        return 5857;
    }

    @Override
    public int getSpecialButton() {
        return 7737;
    }

    @Override
    public int getSpecialBar() {
        return 7749;
    }

    @Override
    public int getSpecialMeter() {
        return 7761;
    }

    @Override
    public FightStance[] getStances() {
        return stances;
    }

}
