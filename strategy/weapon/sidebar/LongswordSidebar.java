package com.osroyale.game.world.entity.combat.strategy.weapon.sidebar;

import com.osroyale.game.world.entity.actor.player.Player;
import com.osroyale.game.world.entity.combat.formula.stance.FightStance;
import com.osroyale.game.world.entity.combat.formula.stance.FightStyle;
import com.osroyale.game.world.entity.combat.formula.stance.FightType;
import com.osroyale.game.world.entity.combat.strategy.weapon.WeaponSidebar;

public class LongswordSidebar extends WeaponSidebar {

    private static final FightStance[] stances = {
        new FightStance(2429, 43, 0, FightType.SLASH, FightStyle.ACCURATE),
        new FightStance(2432, 43, 1, FightType.SLASH, FightStyle.AGGRESSIVE),
        new FightStance(2431, 43, 2, FightType.STAB, FightStyle.CONTROLLED),
        new FightStance(2430, 43, 3, FightType.SLASH, FightStyle.DEFENSIVE)
    };

    public LongswordSidebar(Player player) {
        super(player);
    }

    @Override
    public int getInterfaceId() {
        return 2423;
    }

    @Override
    public int getWeaponTextId() {
        return 2426;
    }

    @Override
    public int getSpecialButton() {
        return 7587;
    }

    @Override
    public int getSpecialBar() {
        return 7599;
    }

    @Override
    public int getSpecialMeter() {
        return 7611;
    }

    @Override
    public FightStance[] getStances() {
        return stances;
    }

}
