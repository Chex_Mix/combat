package com.osroyale.game.world.entity.combat.strategy.weapon;

import com.osroyale.Config;
import com.osroyale.game.world.entity.combat.formula.stance.FightStance;
import com.osroyale.game.world.entity.actor.player.Player;
import com.osroyale.game.world.items.Item;
import com.osroyale.net.packet.out.SendConfig;
import com.osroyale.net.packet.out.SendInterfaceLayer;
import com.osroyale.net.packet.out.SendString;

public abstract class WeaponSidebar {
    private boolean specialActive;
    private final Player player;

    protected WeaponSidebar(Player player) {
        this.player = player;
    }

    public boolean checkButtons(int button) {
        if (button == getSpecialButton()) {
            toggleSpecialBar();
            return true;
        }

        for (FightStance stance : getStances()) {
            if (button == stance.getButton()) {
                sendStanceConfig(stance);
                player.getCombat().setFightType(stance.getFightType());
                player.getCombat().setFightStyle(stance.getFightStyle());
                return true;
            }
        }

        return false;
    }

    public void setSidebar() {
        Item weapon = player.equipment.getWeapon();
        sendWeaponName(weapon == null ? "<col=ff7000>Unarmed" : weapon.getName());
        toggleSpecialBar(false);
        sendSpecialBar(false); // TODO: hide if no spec bar
        sendSidebar();
    }

    private void sendWeaponName(String name) {
        player.send(new SendString(name, getWeaponTextId()));
    }

    private void sendStanceConfig(FightStance stance) {
        player.send(new SendConfig(stance.getParentConfig(), stance.getChildConfig()));
    }

    private void sendSpecialBar(boolean hide) {
        player.send(new SendInterfaceLayer(getSpecialBar(), hide));
    }

    private void toggleSpecialBar() {
        toggleSpecialBar(!specialActive);
    }

    private void toggleSpecialBar(boolean active) {
        if (specialActive == active) {
            return;
        }

        player.send(new SendConfig(301, specialActive = active));
    }

    private void sendSidebar() {
        player.interfaceManager.setSidebar(Config.ATTACK_TAB, getInterfaceId());
    }

    public boolean isSpecialActive() {
        return specialActive;
    }

    public abstract int getInterfaceId();

    public abstract int getWeaponTextId();

    public abstract int getSpecialButton();

    public abstract int getSpecialBar();

    public abstract int getSpecialMeter();

    public abstract FightStance[] getStances();
}
