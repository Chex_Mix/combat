package com.osroyale.game.world.entity.combat.strategy.player;

import com.osroyale.game.Animation;
import com.osroyale.game.world.entity.combat.formula.CombatType;
import com.osroyale.game.world.entity.combat.hit.CombatHit;
import com.osroyale.game.world.entity.combat.hit.Hit;
import com.osroyale.game.world.entity.combat.strategy.BasicStrategy;
import com.osroyale.game.world.entity.actor.Actor;
import com.osroyale.game.world.entity.actor.player.Player;

public class PlayerMagicStrategy extends BasicStrategy<Player> {

    @Override
    protected CombatHit[] getHits(Player attacker, Actor defender) {
        return new CombatHit[] { nextMagicHit(attacker, defender) };
    }

    @Override
    public void start(Player attacker, Actor defender, Hit[] hits) {
        attacker.animate(new Animation(1658));
    }

    @Override
    public int getAttackCooldown(Player attacker) {
        return 4;
    }

    @Override
    public int getAttackDistance(Player attacker) {
        return 1;
    }

    @Override
    public CombatType getCombatType() {
        return CombatType.MAGIC;
    }

}
