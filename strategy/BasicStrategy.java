package com.osroyale.game.world.entity.combat.strategy;

import com.osroyale.game.world.World;
import com.osroyale.game.world.entity.actor.Actor;
import com.osroyale.game.world.entity.combat.CombatUtil;
import com.osroyale.game.world.entity.combat.formula.FormulaFactory;
import com.osroyale.game.world.entity.combat.hit.CombatHit;
import com.osroyale.game.world.entity.combat.hit.Hit;
import com.osroyale.game.world.entity.combat.hit.HitTask;

public abstract class BasicStrategy<T extends Actor> implements CombatStrategy<T> {

    @Override
    public boolean canExecute(T attacker, Actor defender) {
        if (!CombatUtil.canAttack(attacker, defender)) {
            attacker.getCombat().reset();
            attacker.movement.reset();
            return false;
        }

        return attacker.getCombat().cooldown() == 0;
    }

    @Override
    public void execute(T attacker, Actor defender) {
        CombatHit[] hits = getHits(attacker, defender);
        start(attacker, defender, hits);

        for (CombatHit hit : hits) {
            World.schedule(new HitTask<>(attacker, defender, hit, this));
        }

        defender.getCombat().setLastAttacker(attacker);
        attacker.getCombat().cooldown(getAttackCooldown(attacker));
    }

    @Override
    public void hitsplat(T attacker, Actor defender, Hit hit) {
        defender.damage(hit);

        if (defender.getCombat().isRetaliate()) {
            defender.getCombat().target(attacker);
        }
    }

    protected abstract CombatHit[] getHits(T attacker, Actor defender);

    public abstract int getAttackCooldown(T attacker);

    protected CombatHit nextMeleeHit(T attacker, Actor defender) {
        int max = FormulaFactory.getMaxHit(attacker, defender, getCombatType().getFormula());
        Hit hit = FormulaFactory.nextMeleeHit(attacker, defender, getCombatType(), max);
        return new CombatHit(hit, 1);
    }

    protected CombatHit nextRangedHit(T attacker, Actor defender) {
        int max = FormulaFactory.getMaxHit(attacker, defender, getCombatType().getFormula());
        Hit hit = FormulaFactory.nextRangedHit(attacker, defender, getCombatType(), max);
        return new CombatHit(hit, 1);
    }

    protected CombatHit nextMagicHit(T attacker, Actor defender) {
        int max = FormulaFactory.getMaxHit(attacker, defender, getCombatType().getFormula());
        Hit hit = FormulaFactory.nextMagicHit(attacker, defender, getCombatType(), max);
        return new CombatHit(hit, 1);
    }

}
