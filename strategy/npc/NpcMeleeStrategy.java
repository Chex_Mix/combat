package com.osroyale.game.world.entity.combat.strategy.npc;

import com.osroyale.game.world.entity.actor.Actor;
import com.osroyale.game.world.entity.combat.formula.CombatType;
import com.osroyale.game.world.entity.combat.hit.CombatHit;
import com.osroyale.game.world.entity.combat.hit.Hit;
import com.osroyale.game.world.entity.combat.strategy.BasicStrategy;
import com.osroyale.game.world.entity.actor.npc.Npc;

public class NpcMeleeStrategy extends BasicStrategy<Npc> {

    @Override
    protected CombatHit[] getHits(Npc attacker, Actor defender) {
        return new CombatHit[] { nextMeleeHit(attacker, defender) };
    }

    @Override
    public void start(Npc attacker, Actor defender, Hit[] hits) {
        attacker.animate(attacker.definition.getAttackAnimation());
    }

    @Override
    public int getAttackCooldown(Npc attacker) {
        return attacker.definition.getAttackDelay();
    }

    @Override
    public int getAttackDistance(Npc attacker) {
        return attacker.definition.getAttackRadius();
    }

    @Override
    public CombatType getCombatType() {
        return CombatType.MELEE;
    }

}
