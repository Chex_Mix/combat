package com.osroyale.game.world.entity.combat.hit;

import com.osroyale.game.task.Task;
import com.osroyale.game.world.entity.combat.strategy.CombatStrategy;
import com.osroyale.game.world.entity.actor.Actor;

public class HitTask<T extends Actor> extends Task {
    private final T attacker;
    private final Actor defender;
    private final CombatHit hit;
    private final CombatStrategy<T> strategy;

    public HitTask(T attacker, Actor defender, CombatHit hit, CombatStrategy<T> strategy) {
        super(false, hit.getHitsplatDelay());
        this.attacker = attacker;
        this.defender = defender;
        this.hit = hit;
        this.strategy = strategy;
    }

    @Override
    protected void execute() {
        strategy.hitsplat(attacker, defender, hit);
        cancel();
    }
}