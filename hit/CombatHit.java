package com.osroyale.game.world.entity.combat.hit;

import java.util.function.Function;

/**
 * A wrapper for a {@link Hit} object, adding an additional variable for the hitsplat delay.
 *
 * @author Michael | Chex
 */
public final class CombatHit extends Hit {

    /** The hitsplat delay. */
    private final int hitsplatDelay;

    /** Constructs a new {@link CombatHit} object. */
    public CombatHit(Hit hit, int hitsplatDelay) {
        super(hit.getDamage(), hit.getHitsplat(), hit.getHitIcon(), hit.isAccurate());
        this.hitsplatDelay = hitsplatDelay;
    }

    /**
     * Copies and modifies this combat hit.
     *
     * @param modifier the damage modification
     * @return a copy of this combat hit with the damage modifier applied
     */
    public CombatHit copyAndModify(Function<Integer, Integer> modifier) {
        CombatHit next = new CombatHit(this, hitsplatDelay);
        next.modifyDamage(modifier);
        return next;
    }

    /** @return the hitsplat delay. */
    public int getHitsplatDelay() {
        return hitsplatDelay;
    }

}
